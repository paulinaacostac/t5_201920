package view;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{
	}

	public void printMenu()
	{
		System.out.println("1. Cargar archivo CSV");
		System.out.println("2. Prueba de 10.000 llaves SetGet()");
		System.out.println("3. Req1");
		System.out.println("4. Req2");
		System.out.println("5. Exit");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void printCargarViajes(int pViajesTrim1, Viaje pPrimerViajeTrim1, Viaje pUltimoViajeTrim1,int pViajesTrim2, Viaje pPrimerViajeTrim2, Viaje pUltimoViajeTrim2,int pViajesTrim3, Viaje pPrimerViajeTrim3, Viaje pUltimoViajeTrim3)
	{
		System.out.println(
				"Trimestre 1:"+"\n"
						+ " Numero de viajes cargados: "+pViajesTrim1+"\n"+
						"Primer Viaje Cargado: "+"Dstid: "+pPrimerViajeTrim1.darDstid()+" Sourceid: "+pPrimerViajeTrim1.darSourceid()+" Mean_Travel_Time: "+pPrimerViajeTrim1.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pPrimerViajeTrim1.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pPrimerViajeTrim1.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pPrimerViajeTrim1.darGeometricStandardDeviationTravelTime()+"\n"+
						"Ultimo Viaje Cargado: "+"Dstid: "+pUltimoViajeTrim1.darDstid()+" Sourceid: "+pUltimoViajeTrim1.darSourceid()+" Mean_Travel_Time: "+pUltimoViajeTrim1.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pUltimoViajeTrim1.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pUltimoViajeTrim1.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pUltimoViajeTrim1.darGeometricStandardDeviationTravelTime()+"\n"+
						"Trimestre 2:"+"\n"
						+ " Numero de viajes cargados: "+pViajesTrim2+"\n"+
						"Primer Viaje Cargado: "+"Dstid: "+pPrimerViajeTrim2.darDstid()+" Sourceid: "+pPrimerViajeTrim2.darSourceid()+" Mean_Travel_Time: "+pPrimerViajeTrim2.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pPrimerViajeTrim2.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pPrimerViajeTrim2.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pPrimerViajeTrim2.darGeometricStandardDeviationTravelTime()+"\n"+
						"Ultimo Viaje Cargado: "+"Dstid: "+pUltimoViajeTrim2.darDstid()+" Sourceid: "+pUltimoViajeTrim2.darSourceid()+" Mean_Travel_Time: "+pUltimoViajeTrim2.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pUltimoViajeTrim2.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pUltimoViajeTrim2.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pUltimoViajeTrim2.darGeometricStandardDeviationTravelTime()+"\n"+
						"Trimestre 3:"+"\n"
						+ " Numero de viajes cargados: "+pViajesTrim3+"\n"+
						"Primer Viaje Cargado: "+"Dstid: "+pPrimerViajeTrim3.darDstid()+" Sourceid: "+pPrimerViajeTrim3.darSourceid()+" Mean_Travel_Time: "+pPrimerViajeTrim3.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pPrimerViajeTrim3.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pPrimerViajeTrim3.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pPrimerViajeTrim3.darGeometricStandardDeviationTravelTime()+"\n"+
						"Ultimo Viaje Cargado: "+"Dstid: "+pUltimoViajeTrim3.darDstid()+" Sourceid: "+pUltimoViajeTrim3.darSourceid()+" Mean_Travel_Time: "+pUltimoViajeTrim3.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pUltimoViajeTrim3.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pUltimoViajeTrim3.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pUltimoViajeTrim3.darGeometricStandardDeviationTravelTime()+"\n");
	}
	public void printTablaCargar(int nLinear,int tamInicialLinear,int tamFinalLinear, int numRehashLinear,int nChaining,int tamInicialChaining,int tamFinalChaining, int numRehashChaining, double lfLinear, double lfChaining) 
	{
		System.out.println("Linear Probing");
		System.out.println("Numero de duplas (K,V) en la tabla: N= "+nLinear);
		System.out.println("Tama�o inicial del arreglo: M inicial= "+tamInicialLinear);
		System.out.println("Tama�o final de arreglo: M final= "+tamFinalLinear);
		System.out.println("Factor de carga: N/M= " + lfLinear );
		System.out.println("Numero de rehashes = "+numRehashLinear);
		
		System.out.println("SeparateChaining");
		System.out.println("Numero de duplas (K,V) en la tabla: N= "+nChaining);
		System.out.println("Tama�o inicial del arreglo: M inicial= "+tamInicialChaining);
		System.out.println("Tama�o final de arreglo: M final= "+tamFinalChaining);
		System.out.println("Factor de carga: N/M = " + lfChaining);
		System.out.println("Numero de rehashes = "+numRehashChaining);
	}


	public void printAnalisisTiempo(long a,long b, double c, long d, long e, double f)
	{
		System.out.println("Linear Probing:");
		System.out.println("El tiempo maximo getSet(): " + b);
		System.out.println("El tiempo minimo getSet(): " + a);
		System.out.println("El tiempo promedio getSet(): " + c);
		System.out.println("Separate Chaining: ");
		System.out.println("El tiempo maximo getSet(): " + e);
		System.out.println("El tiempo minimo getSet(): " + d);
		System.out.println("El tiempo promedio getSet(): " + f);
		System.out.println("--------- \n");
	}
	
	public void printBuscar(ArregloDinamico<Viaje> viajes, String pHashTable, int pTrimestre)
	{
		if (viajes.darTamano()==0) {
			System.out.println("No hay viajes que correspondan con la busqueda.");
			System.out.println("--------- \n");
			return;
		}
		System.out.println("Viajes retornados usando " + pHashTable);
		System.out.println("trimestre| sourceid| dstid| dow| mean_travel_time");
		for (int i = 0; i < viajes.darTamano(); i++) {
			Viaje viaje = viajes.darElemento(i);
			System.out.println(pTrimestre + "|" + viaje.darSourceid()+ "|" + viaje.darDstid() + "|" + viaje.darDow() + "|" +viaje.darMeanTravelTime());
		}
		System.out.println("--------- \n");
	}


}
