package controller;


import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
	}

	public void run() 
	{
		int tamanoInicial = 4001;
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		ArregloDinamico<Viaje> viajesHora=null;
		String dato2 = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				modelo = new MVCModelo(tamanoInicial);
				modelo.cargar();
				System.out.println("Archivo CSV cargado");
				view.printCargarViajes(modelo.darViajesTrim1(), modelo.darPrimerViajeTrim1(), modelo.darUltimoViajeTrim1(),modelo.darViajesTrim2(), modelo.darPrimerViajeTrim2(), modelo.darUltimoViajeTrim2(),modelo.darViajesTrim3(), modelo.darPrimerViajeTrim3(), modelo.darUltimoViajeTrim3());
				System.out.println("Tabla Cargar");
				view.printTablaCargar(modelo.darViajesTrim1()+modelo.darViajesTrim2()+modelo.darViajesTrim3()+modelo.darViajesTrim4(), modelo.darTamanoInicial(), modelo.darTamanoFinalLinear(), modelo.darNumRehashLinear(), modelo.darViajesTrim1()+modelo.darViajesTrim2()+modelo.darViajesTrim3()+modelo.darViajesTrim4(), modelo.darTamanoInicial(), modelo.darTamanoFinalChaining(), modelo.darNumRehashChaining(), modelo.darLoadingFactorLinear(), modelo.darLoadingFactorChaining());
				break;
			case 2:
				System.out.println("--------- \nCalculando tiempos");
				modelo.pruebaConsultasChaining();
				modelo.pruebaConsultasLinear();
				view.printAnalisisTiempo(modelo.darMinPruebaLinear(),modelo.darMaxPruebaLinear(),modelo.darPromPruebaChaining(),modelo.darMinPruebaChaining(),modelo.darMaxPruebaChaining(),modelo.darPromPruebaChaining());
				break;
			case 3:
				System.out.println("Escriba el trimestre:");
				int trimestre = lector.nextInt();
				System.out.println("Escriba sourceid:");
				int sourceid = lector.nextInt();
				System.out.println("Escriba destid:");
				int destid = lector.nextInt();
				view.printBuscar(modelo.darTiemposViajeLinear(trimestre, sourceid, destid), "Linear Probing", trimestre);
				break;
			case 4:
				System.out.println("Escriba el trimestre:");
				int trimestre1 = lector.nextInt();
				System.out.println("Escriba sourceid:");
				int sourceid1 = lector.nextInt();
				System.out.println("Escriba destid:");
				int destid1 = lector.nextInt();
				view.printBuscar(modelo.darTiemposViajeChaining(trimestre1, sourceid1, destid1), "Separate Chaining", trimestre1);
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
