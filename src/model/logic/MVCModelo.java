
package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateChainingHashST;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private Viaje primerViajeTrim1;
	private Viaje primerViajeTrim2;
	private Viaje primerViajeTrim3;
	private Viaje primerViajeTrim4;
	private Viaje ultimoViajeTrim1;
	private Viaje ultimoViajeTrim2;
	private Viaje ultimoViajeTrim3;
	private Viaje ultimoViajeTrim4;
	private int viajesTrim1;
	private int viajesTrim2;
	private int viajesTrim3;
	private int viajesTrim4;
	private int tamanoInicial;
	private int tamanoFinalLinear;
	private int numRehashLinear;
	private int numRehashChaining;
	private int tamanoFinalChaining;
	private long minPruebaLinear;
	private long maxPruebaLinear;
	private double promPruebaLinear;
	private long minPruebaChaining;
	private long maxPruebaChaining;
	private double promPruebaChaning;
	private LinearProbingHashST<String, Viaje> STLinear;
	private SeparateChainingHashST<String,Viaje> STChaining;

	public MVCModelo(int ptamanoInicial)
	{
		primerViajeTrim1=new Viaje(0, 0, 0, 0, 0, 0,0);
		primerViajeTrim2=new Viaje(0, 0, 0, 0, 0, 0,0);
		primerViajeTrim3=new Viaje(0, 0, 0, 0, 0, 0,0);
		primerViajeTrim4=new Viaje(0, 0, 0, 0, 0, 0,0);
		ultimoViajeTrim1=new Viaje(0, 0, 0, 0, 0, 0,0);
		ultimoViajeTrim2=new Viaje(0, 0, 0, 0, 0, 0,0);
		ultimoViajeTrim3=new Viaje(0, 0, 0, 0, 0, 0,0);
		ultimoViajeTrim4=new Viaje(0, 0, 0, 0, 0, 0,0);
		minPruebaChaining=Long.MAX_VALUE;
		minPruebaChaining=Long.MAX_VALUE;
		maxPruebaChaining=0;
		minPruebaChaining=0;
		promPruebaChaning=0.0;
		promPruebaLinear=0.0;
		tamanoInicial=ptamanoInicial;
		STLinear=new LinearProbingHashST<String,Viaje>();
		STChaining=new SeparateChainingHashST<>();
	}
	public void cargar()
	{
		CSVReader reader = null;
		CSVReader reader1 = null;
		CSVReader reader2 = null;
		CSVReader reader3 = null;
		Viaje viaje=null;
		String keyViaje=null;
		LinearProbingHashST<String, Viaje> STLinearTrim1 = new LinearProbingHashST<String,Viaje>(tamanoInicial);
		LinearProbingHashST<String, Viaje> STLinearTrim2 = new LinearProbingHashST<String,Viaje>(tamanoInicial);
		LinearProbingHashST<String, Viaje> STLinearTrim3 = new LinearProbingHashST<String,Viaje>(tamanoInicial);
		LinearProbingHashST<String, Viaje> STLinearTrim4 = new LinearProbingHashST<String,Viaje>(tamanoInicial);
		try 
		{
			//			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			//			reader1 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-WeeklyAggregate.csv"));
			//			reader2 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-3-WeeklyAggregate.csv"));
			reader3 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-4-WeeklyAggregate.csv"));
			//			reader.skip(1);
			//			reader1.skip(1);
			//			reader2.skip(1);
			reader3.skip(1);
			//Trimestre 1
			//			for(String[] nextLine : reader) 
			//			{
			//				keyViaje=nextLine[0]+"-"+ nextLine[1]+"-"+"1";
			//				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
			//				if (STLinearTrim1.isEmpty())
			//				{
			//					primerViajeTrim1=viaje;
			//				}
			//				STLinearTrim1.putInSet(keyViaje, viaje);
			//				STLinear.putInSet(keyViaje, viaje);
			//				STChaining.putInSet(keyViaje, viaje);
			//				ultimoViajeTrim1=viaje;
			//				viajesTrim1++;
			//			}
			//			//Trimestre 2
			//			for(String[] nextLine : reader1) 
			//			{
			//				keyViaje=nextLine[0]+"-"+ nextLine[1]+"-"+"2";
			//				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
			//				if (STLinearTrim2.isEmpty())
			//				{
			//					primerViajeTrim2=viaje;
			//				}
			//				STLinearTrim2.putInSet(keyViaje, viaje);
			//				STLinear.putInSet(keyViaje, viaje);
			//				STChaining.putInSet(keyViaje, viaje);
			//				ultimoViajeTrim2=viaje;
			//				viajesTrim2++;
			//			}
			//			//Trimestre 3
			//			for(String[] nextLine : reader2) 
			//			{
			//				keyViaje=nextLine[0]+"-"+ nextLine[1]+"-"+"3";
			//				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
			//
			//				if (STLinearTrim3.isEmpty())
			//				{
			//					primerViajeTrim3=viaje;
			//				}
			//				STLinearTrim3.putInSet(keyViaje, viaje);
			//				STLinear.putInSet(keyViaje, viaje);
			//				STChaining.putInSet(keyViaje, viaje);
			//				ultimoViajeTrim3=viaje;
			//				viajesTrim3++;
			//			}
			//Trimestre 4
			for(String[] nextLine : reader3) 
			{
				keyViaje=nextLine[0]+"-"+ nextLine[1]+"-"+"4";
				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				if (STLinearTrim4.isEmpty())
				{
					primerViajeTrim4=viaje;
				}
				STLinearTrim4.putInSet(keyViaje, viaje);
				STLinear.putInSet(keyViaje, viaje);
				STChaining.putInSet(keyViaje, viaje);
				ultimoViajeTrim4=viaje;
				viajesTrim4++;
			}
			tamanoFinalLinear=STLinear.darM();
			tamanoFinalChaining=STChaining.darM();
			numRehashLinear=STLinear.darNumRehash();
			numRehashChaining=STChaining.darNumRehash();
		}
		catch (FileNotFoundException e)  
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try {
			reader3.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int darTamanoInicial()
	{
		return tamanoInicial;
	}
	public int darTamanoFinalLinear()
	{
		return tamanoFinalLinear;
	}
	public int darTamanoFinalChaining()
	{
		return tamanoFinalChaining;
	}
	public int darNumRehashLinear()
	{
		return numRehashLinear;
	}
	public int darNumRehashChaining()
	{
		return numRehashChaining;
	}
	public Viaje darPrimerViajeTrim1()
	{
		return primerViajeTrim1;
	}
	public Viaje darPrimerViajeTrim2()
	{
		return primerViajeTrim2;
	}
	public Viaje darPrimerViajeTrim3()
	{
		return primerViajeTrim3;
	}
	public Viaje darPrimerViajeTrim4()
	{
		return primerViajeTrim4;
	}
	public Viaje darUltimoViajeTrim1()
	{
		return ultimoViajeTrim1;
	}
	public Viaje darUltimoViajeTrim2()
	{
		return ultimoViajeTrim2;
	}
	public Viaje darUltimoViajeTrim3()
	{
		return ultimoViajeTrim3;
	}
	public Viaje darUltimoViajeTrim4()
	{
		return ultimoViajeTrim4;
	}
	public int darViajesTrim1()
	{
		return viajesTrim1;
	}
	public int darViajesTrim2()
	{
		return viajesTrim2;
	}
	public int darViajesTrim3()
	{
		return viajesTrim3;
	}
	public int darViajesTrim4()
	{
		return viajesTrim4;
	}
	public long darMinPruebaLinear()
	{
		return minPruebaLinear;
	}
	public long darMaxPruebaLinear()
	{
		return maxPruebaLinear;
	}
	public double darPromPruebaLinear()
	{
		return promPruebaLinear;
	}
	public long darMinPruebaChaining()
	{
		return minPruebaChaining;
	}
	public long darMaxPruebaChaining()
	{
		return maxPruebaChaining;
	}
	public double darPromPruebaChaining()
	{
		return promPruebaChaning;
	}

	public double darLoadingFactorLinear()
	{
		return (double)STLinear.darN()/(double)STLinear.darM();
	}

	public double darLoadingFactorChaining()
	{
		return (double)STChaining.darN()/(double)STChaining.darM();
	}
	public ArregloDinamico<String> darLlavesExistentes()
	{
		ArregloDinamico<String> arreglo=new ArregloDinamico<String>(8000);
		int contador=0;
		Iterator<String> iter=STChaining.keys();
		while(iter.hasNext() && contador<8000)
		{
			String sig=iter.next();
			arreglo.agregar(sig);
			contador++;
		}
		return arreglo;
	}
	public ArregloDinamico<String> darLlavesNoExistentes()
	{
		ArregloDinamico<String> arreglo=new ArregloDinamico<String>(2000);
		for (int i=0;i<2000;i++)
		{
			arreglo.agregar(""+i);		
		}
		return arreglo;
	}

	public void pruebaConsultasLinear()
	{
		long startTime=0;
		long endTime=0;
		long duracion=0;
		double n=0.0;
		long duracionTotal=0;
		for (int i=0;i<8000;i++)
		{
			ArregloDinamico<String> llavesExistentes=darLlavesExistentes();
			startTime =System.currentTimeMillis();
			STLinear.get(llavesExistentes.darElemento(i));
			endTime =System.currentTimeMillis();
			duracion = (endTime-startTime);
			if (duracion > maxPruebaLinear)
			{
				maxPruebaLinear=duracion;
			}
			if (duracion < minPruebaLinear)
			{
				minPruebaLinear=duracion;
			}
			duracionTotal=duracionTotal+duracion;
			n++;
		}
		for (int i=0;i<2000;i++)
		{
			startTime = System.currentTimeMillis();
			STLinear.get(darLlavesNoExistentes().darElemento(i));
			endTime = System.currentTimeMillis();
			duracion = (endTime-startTime);
			if (duracion > maxPruebaLinear)
			{
				maxPruebaLinear=duracion;
			}
			if (duracion < minPruebaLinear)
			{
				minPruebaLinear=duracion;
			}
			duracionTotal=duracionTotal+duracion;
			n++;
		}
		promPruebaLinear=((double)duracionTotal)/n;
	}
	public void pruebaConsultasChaining()
	{
		long startTime=0;
		long endTime=0;
		long duracion=0;
		double n=0.0;
		long duracionTotal=0;
		for (int i=0;i<8000;i++)
		{
			ArregloDinamico<String> llavesExistentes=darLlavesExistentes();
			startTime = System.currentTimeMillis();
			STChaining.get(llavesExistentes.darElemento(i));
			endTime = System.currentTimeMillis();
			duracion = (endTime-startTime);
			if (duracion > maxPruebaChaining)
			{
				maxPruebaChaining=duracion;
			}
			if (duracion < minPruebaChaining)
			{
				minPruebaChaining=duracion;
			}
			duracionTotal=duracionTotal+duracion;
			n++;
		}
		for (int i=0;i<2000;i++)
		{
			startTime = System.currentTimeMillis();
			STChaining.get(darLlavesNoExistentes().darElemento(i));
			endTime = System.currentTimeMillis();
			duracion = (endTime-startTime);
			if (duracion > maxPruebaChaining)
			{
				maxPruebaChaining=duracion;
			}
			if (duracion < minPruebaChaining)
			{
				minPruebaChaining=duracion;
			}
			duracionTotal=duracionTotal+duracion;
			n++;
		}
		promPruebaChaning=duracionTotal/n;
	}

	public ArregloDinamico<Viaje> darTiemposViajeLinear(int pTrimestre, int pSourceid, int pDestid)
	{
		ArregloDinamico<Viaje> viajes = new ArregloDinamico<Viaje>(7);
		String key = pSourceid + "-" + pDestid + "-" + pTrimestre;
		Iterator<Viaje> viajesIt= STLinear.getSet(key);
		while (viajesIt.hasNext()) {
			Viaje viaje = (Viaje) viajesIt.next();
			viajes.agregar(viaje);
		}
		ordenarPorDow(viajes);
		return viajes;
	}

	public ArregloDinamico<Viaje> darTiemposViajeChaining(int pTrimestre, int pSourceid, int pDestid)
	{
		ArregloDinamico<Viaje> viajes = new ArregloDinamico<Viaje>(7);
		String key = pSourceid + "-" + pDestid + "-" + pTrimestre;
		Iterator<Viaje> viajesIt= STChaining.getSet(key);
		while (viajesIt.hasNext()) {
			Viaje viaje = (Viaje) viajesIt.next();
			viajes.agregar(viaje);
		}
		ordenarPorDow(viajes);
		return viajes;
	}

	public void ordenarPorDow(ArregloDinamico<Viaje> viajes)
	{
		for (int i = 1; i < viajes.darTamano(); i++) 
		{
			for (int j = i; j > 0 && viajes.darElemento(j).darDow()<viajes.darElemento(j-1).darDow(); j--) 
			{
				viajes.exch(j, j-1);
			}
		}
	}


}
